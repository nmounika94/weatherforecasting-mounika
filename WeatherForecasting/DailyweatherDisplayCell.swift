//
//  DailyweatherDisplayCell.swift
//  WeatherForecast
//
//  Created by Mounika Nerella on 9/13/17.
//  Copyright © 2017 Naveen Magatala. All rights reserved.
//

import UIKit

class DailyweatherDisplayCell: UITableViewCell {
    
    
    @IBOutlet weak var weatherImg: UIImageView!

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var summary: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
