//
//  DisplayingTableViewController.swift
//  WeatherForecasting
//
//  Created by Mounika Nerella on 9/12/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class DisplayingTableViewController: UITableViewController,UISearchResultsUpdating,UISearchBarDelegate {
    
    
    let searchController = UISearchController(searchResultsController: nil)
    var city : String?
    var currentArray = [Currentlyweather]()
    //searchController.searchResultsUpdater = self
//    tableView.tableHeaderView = searchController.searchBar
//    searchController.dimsBackgroundDuringPresentation = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        var info =  LocationInfo()
        info.lattitude = "58.89"
        info.longitude = "111.11"
        
//        let url = JsonParsing.getAPI(loc: info)
//        
//        
//        JsonParsing.getDarkSkyData(url: url) { (current, Daily) in
//            
//            //dailyArray is prepared with all the values we got from json Parsing.
//            
//            //we need to get main thread and update our UI.
//            //here the tableview data should be reloaded.
//            
//        }
        
            
        
        searchController.searchResultsUpdater = self
        self.tableView.tableHeaderView = self.searchController.searchBar
       //tableView.tableHeaderView = searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        
    }
        
    
    
    func updateSearchResults(for searchController: UISearchController) {
        
        
        
    }
    
    //using searchbar i enter zipcode and get the name of the place
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        let loc = parsedDarkSkyData.getLatLong(zipcode: searchBar.text!)
        
        let url = JsonParsing.getAPI(loc: loc)
        JsonParsing.getDarkSkyData(url: url) { (current, daily) in
            DispatchQueue.main.async {
                currentDayWeather =  current
                dailyDayWeather = daily
                print(currentDayWeather ?? "")
                print(dailyDayWeather?.first?.hunidity ?? "")
                self.currentArray.append(currentDayWeather!)
                self.tableView.reloadData()
            }
        }
//        parsedDarkSkyData.reversegeoCodeLocation(location: loc) { locality in
//            self.city = locality
        
       // }
    }
    
    func getWeatherImage(summary : String) -> String {
        
        let weatherCondition = Weather.Condition(rawValue: summary)
            
            let str = (weatherCondition?.title)!
            if (str != ""){
                return str
            }else {

            return "frosted"
        }
        
       
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return currentArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentweatherCell", for: indexPath) as? CurrentweatherCell
        let currObj = currentArray[indexPath.row]
        if let ts = currObj.timeStamp
        {
            let time = Date(timeIntervalSince1970: Double(ts)!)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMMM YYYY"
            let strDate = formatter.string(from: time)
            cell?.dateLabel.text = strDate
        }
        //print(getWeatherImage(summary: (currentDayWeather?.summary)!))
      // cell?.weatherImg.image = getWeatherImage(summary: (currentDayWeather?.summary)!)
        let imgStr = getWeatherImage(summary: (currObj.summary)!)
        cell?.weatherImg.image = UIImage(named: imgStr)
        cell?.humidityLabel.text = currentDayWeather?.humidity
        cell?.summaryLabel.text = currentDayWeather?.summary
        cell?.temperatureLabel.text = currentDayWeather?.temperature
        cell?.windSpeedLabel.text = currentDayWeather?.windSpeed

        return cell!
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
