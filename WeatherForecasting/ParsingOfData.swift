//
//  ParsingOfData.swift
//  WeatherForecasting
//
//  Created by Mounika Nerella on 9/11/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
import CoreLocation

typealias  localityCompletionHandler = (_ : String) -> Void

typealias weatherCompletionHandler = (_ : Currentlyweather, _: [DailyWeather]) -> Void
 var dailyArray = [DailyWeather]()

class parsedDarkSkyData {
    static func readLocationData(){
        guard let csvPath = Bundle.main.path(forResource: "locationsData", ofType: "csv")else{ return
        }
        do {
            let data = try String(contentsOfFile: csvPath, encoding: .utf8)
            let rows = data.components(separatedBy: "\n")
            print(rows[1])
            for row in rows{
                
                if row == " " {
                    return
                }
                if row != ""{
                let eachComponent = row.components(separatedBy: ",")
               
                let long = eachComponent[2].replacingOccurrences(of: " ", with: "")
                let dataObj = CsvData(zip: eachComponent[0], longitutude: long, lattitude: eachComponent[1])
                //print(dataObj[0])
                
                dataArray.append(dataObj)
                }
            }
        }
        catch{}
    }
    
    static func getLatLong(zipcode: String) -> LocationInfo  {
        
        
        var location: LocationInfo?
        for values in dataArray{
            
            if values.zip == zipcode {
                
                location = LocationInfo(lattitude: values.lattitude, longitude: values.longitutude)
                   print(values.zip)
//                 print(lattitude)
//                 print(longitude)
            }
            
        }
       
       
     return location!
    }
    
    
    //reverse geocode is used for getting the place name by using lattitude and longitude values
    
    static func reversegeoCodeLocation(location: LocationInfo, completion: @escaping localityCompletionHandler) {
        
        var locality: String?
        let loc = CLLocation(latitude: Double(location.lattitude!)!, longitude: Double(location.longitude!)!)
        CLGeocoder().reverseGeocodeLocation(loc) { (placeMarks, error) in
            
            if error != nil{
                print(error?.localizedDescription)
            }
            if let placeMark = placeMarks?.last {
                locality = placeMark.locality
                completion(locality!)
            }
        }
        
    }
   
}

class JsonParsing {
    static func getAPI(loc: LocationInfo) -> URL{
        
         print(loc)
         let urlStr = String(format: dataSkyUrl, loc.lattitude!,loc.longitude! )
        print(urlStr)
        let url = URL(string: urlStr)
        return url!
    }
    static func getDarkSkyData(url: URL,completion: @escaping weatherCompletionHandler) {
        
        var currentWeatherObj: Currentlyweather?
       
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            do{
                let results = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                for result in results! {
                    if result.key == "currently" {
                        let currently = result.value as? [String: Any]
                        let humidity = currently?["humidity"] as? Double ?? 0
                        let summary = currently? ["summary"] as? String ?? ""
                        let temperature = currently? ["temperature"] as? Double ?? 0
                        let windSpeed = currently? ["windSpeed"] as? Double ?? 0
                        let timeStamp = currently?["time"] as? Int ?? 0
                        
                        print(humidity)
                        print( summary)
                        
                        currentWeatherObj = Currentlyweather(hum: String(humidity), sum: summary, temp: String(temperature), time: String(timeStamp), wind: String(windSpeed))
                    }
                    
                    if result.key == "daily" {
                        let daily = result.value as? [String: Any]
                        //let humidity = cu
                        
                        for number in daily! {
                            if number.key == "data" {
                                let days = number.value as? [[String:Any]]
                                for day in days! {
                                    let humidity = day["humdity"] as? Double ?? 0
                                    let summary = day["summary"] as? String ?? ""
                                    let minTemp = day["temperatureMin"] as? Double ?? 0
                                    let maxTemperature = day["temperatureMax"] as? Double ?? 0
                                    let windSpeed = day["windSpeed"] as? Double ?? 0
                                    let timeStamp = day["time"] as? Int ?? 0
                                    dailyArray.append(DailyWeather(hunidity: String(humidity), summary: summary, minTemperature: String(minTemp), maxTemperature: String(maxTemperature), timeStamp: String(timeStamp), windSpped: String(windSpeed)))
                                    print(dailyArray)
                                    
                                }
                                
                            }
                        }
                    }
                }
                
            }
            catch{}
            completion(currentWeatherObj!, dailyArray)
            
            
        }.resume()
       //print(dailyArray)
    }
    
 
    
}
