//
//  ModelClassValues.swift
//  WeatherForecasting
//
//  Created by Mounika Nerella on 9/11/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
struct CsvData {
    
    var zip: String?
    var longitutude: String?
    var lattitude: String?
}

struct LocationInfo {
    var lattitude: String?
    var longitude: String?
}

class Currentlyweather {

     var humidity : String?
    var summary : String?
    var temperature : String?
    var timeStamp : String?
    var windSpeed : String?
    
    init (hum : String, sum : String, temp : String, time : String, wind: String) {
        humidity = hum
        summary = sum
        temperature = temp
        timeStamp = time
        windSpeed = wind
    }
    
}

struct DailyWeather {
    var hunidity : String?
    var summary : String?
    var minTemperature : String?
    var maxTemperature : String?
    var timeStamp : String?
    var windSpped : String?
}
