//
//  DailyWeatherTableViewController.swift
//  WeatherForecast
//
//  Created by Mounika Nerella on 9/13/17.
//  Copyright © 2017 Naveen Magatala. All rights reserved.
//

import UIKit
import CoreLocation

class DailyWeatherTableViewController: UITableViewController,UISearchResultsUpdating,UISearchBarDelegate {
    
    let searchController = UISearchController(searchResultsController: nil)
    var city: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchResultsUpdater = self
        tableView.tableHeaderView = searchController.searchBar
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self

            }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        
        
    }
//    func updateWeatherForLocation (location: String) {
//        CLGeocoder().geocodeAddressString(location) { (placemarks, error) in
//            if error == nil {
//                if let location = placemarks?.first?.location {
//                    dailyDayWeather.pars
//                }
//            }
//        }
//        
//    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let loc = parsedDarkSkyData.getLatLong(zipcode: searchBar.text!)
        let url = JsonParsing.getAPI(loc: loc)
        JsonParsing.getDarkSkyData(url: url) { (current, daily) in
            DispatchQueue.main.async {
                dailyDayWeather = daily
                currentDayWeather = current
                print(currentDayWeather ?? "")
                print(dailyDayWeather?.first?.hunidity ?? "")
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let count = dailyDayWeather?.count {
            return count
        }
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailyweatherDisplayCell", for: indexPath) as? DailyweatherDisplayCell
        guard let dailyObj = dailyDayWeather?[indexPath.row] else {
            return cell!
        }
        if let ts = dailyObj.timeStamp {
            
            let time = Date(timeIntervalSince1970 : Double(ts)!)
            let formatter = DateFormatter()
            formatter.dateFormat = "EEEE"
            let strDate = formatter.string(from: time)
            cell?.dayLabel.text = strDate
            
        }
        
        
        //let weatherObj = dailyDayWeather?[indexPath.section]
        cell?.weatherImg.image = getWeatherImage(summary: dailyObj.summary!)
        cell?.humidityLabel.text = dailyObj.hunidity
        cell?.maxTempLabel.text = dailyObj.maxTemperature
        cell?.minTempLabel.text = dailyObj.minTemperature
        cell?.summary.text = dailyObj.summary
        cell?.timeStampLabel.text = dailyObj.timeStamp
        cell?.windSpeedLabel.text = dailyObj.windSpped
        
        return cell!
        }
    
    func getWeatherImage(summary: String) -> UIImage {
        if let weatherCondition = Weather.Condition(rawValue: summary) {
            print(weatherCondition)
            return UIImage(named: weatherCondition.title)!
        }
        return UIImage(named: "frosted")!
    }
//
//    func assignCurrentDayWeather() {
//        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "DailyweatherDisplayCell", for: indexPath) as? DailyweatherDisplayCell
//        humidityLabel.text = currentDayWeather?.hunidity
//        maxTempLabel.text = currentDayWeather?.maxTemperature
//        minTempLabel.text = currentDayWeather?.minTemperature
//        summary.text = currentDayWeather?.summary
//        timeStampLabel.text = currentDayWeather?.timeStamp
//        windSpeedLabel.text = currentDayWeather?.windSpped
//        
//        
//        
//    }

//
//    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        let date = Calendar.current.date(byAdding: .day, value: section, to: Date())
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        return dateFormatter.string(from: date!)
//        
//
//        
//    }
    
    

        // Configure the cell...

    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
